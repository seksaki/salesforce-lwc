import { LightningElement, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';

const fields = [
    'Contact.MailingStreet',
    'Contact.MailingCity',
    'Contact.MailingCountry',
    'Contact.MailingPostalCode',
    'Contact.MailingLatitude',
    'Contact.MailingLongitude'
];

export default class GetDataContact extends LightningElement {
    @api recordId;

    @wire(getRecord, { recordId: '$recordId', fields })
    contact;

    get mailingaddress(){
        return this.contact.data.fields.MailingStreet.value;
    }
    get city(){
        return this.contact.data.fields.MailingCity.value;
    }
    
    get country(){
        return this.contact.data.fields.MailingCountry.value;
    }
    
    get codep(){
        return this.contact.data.fields.MailingPostalCode.value;
    }
    
    get latitude(){
        return this.contact.data.fields.MailingLatitude.value;
    }
    get longitude(){
        return this.contact.data.fields.MailingLongitude.value;
    }
}