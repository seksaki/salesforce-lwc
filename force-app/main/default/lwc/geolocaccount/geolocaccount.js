import { LightningElement ,wire,track} from 'lwc';
import getAccount from '@salesforce/apex/AccountLocation.getAccount';
 
export default class GeoLocAccount extends LightningElement {
     @track accounts;
     @track error;
     @track showFooter = true ;
     @wire(getAccount)
     wiredAccountss({error,data}) {
         if (data) {
             this.accounts = data;
         } else if (error) {
            
             this.error = error;
         }
     }
}
